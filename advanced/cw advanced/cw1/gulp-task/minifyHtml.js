let gulp = require('gulp');
let htmlmin = require('gulp-htmlmin');

function minifyhtml () {
    return gulp.src('src/*.html')
    .pipe(
        htmlmin({
        collapseWhitespace: true
    })
)
    .pipe(gulp.dest('dist'))
}



gulp.task('html', function () {
    return gulp
    .src('src/*.html')
    .pipe(
        htmlmin({
        collapseWhitespace: true
    })
)
.pipe(gulp.dest('dist'));
});

exports.html = minifyhtml;