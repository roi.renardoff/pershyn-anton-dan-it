const gulp = require('gulp');
const gulpConcat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');


function handleStyles () {
    return gulp.src('src/css')
    .pipe(gulpConcat('main.css'))
    .pipe(autoprefixer({
        cascade: false
    }))
    .pipe(cssmin())
    .pipe(rename(min.css))
    .pipe(gulp.dest("dist"));
}


exports.styles = handleStyles;