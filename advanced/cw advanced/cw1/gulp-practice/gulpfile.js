// 0) очистить dist
// 1)минифицировать htmlmin
// 2)обьединить css в один файл
// 3)добавление вендорных векторов
// 4)минифицировать css

const gulp = require('gulp');
const cleardistFolder = require('./gulp-task/cleanDist').cleanDist;
const htmlTask = require('./gulp-task/html').html;
const cssTask = require('./gulp-task/styles').styles;

exports.dev = gulp.series(cleardistFolder, gulp.parallel(htmlTask, cssTask ));


