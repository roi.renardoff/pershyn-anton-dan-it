// У вас есть 2 массива строк, в каждом из них - фамилии клиентов.
// Создайте на их основе один массив, который будет представлять собой объединение двух массив без повторяющихся фамилий клиентов.

const clients1 = [ 'Гилберт', 'Сальваторе', 'Пирс', 'Соммерс', 'Форбс', 'Донован', 'Беннет']

const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон']

const newCompany = [...new Set([...clients1, ...clients2])]

console.log(newCompany)

// const mySet = new Set([...clients1, ...clients2]);
// const newCompany = [...mySet];
// console.log(newCompany);

// function unique (array){
//     let newCompany = []
//     return Array.from(new Set(array));
// }
//
// console.log(unique(newCompany));
