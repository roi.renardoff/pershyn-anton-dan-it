// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). 
// Сделайте так, чтобы эти свойства заполнялись при создании объекта.
// Создайте геттеры и сеттеры для этих свойств.
// Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(newName) {
        if (typeof newName === 'string' || newName !== '') {
            this._name = newName;
        }
    }

    get age() {
        return this._age;
    }
    set age(newAge) {
        if (!isNaN(newAge) || newAge !== '') {
            this._age = newAge;
        }
    }

    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        if (!isNaN(newSalary) || newSalary !== '') {
            this._salary = newSalary;
        }
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super (name, age, salary);
        this._lang = lang;
    }
    get salary () {
        return this._salary*3;
    }

    get lang () {
        return this._lang;
    }
    set lang (newLang) {
        if (typeof newLang === 'string' || newLang !== '') {
            this._lang = newLang;
        }
    }
}

const antonProgrammer = new Programmer('Anton', 23, 20000, 'Ukraine');
console.log(antonProgrammer);
const olyaProgrammer = new Programmer('Olya', 21, 30000, 'English');
console.log(olyaProgrammer);
const alexProgrammer = new Programmer('Alex', 35, 40000, 'Deutschland');
console.log(alexProgrammer);






