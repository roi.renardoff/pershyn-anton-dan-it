// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
// Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

// Вариант 1 (оптимизирован под большое кол-во свойств)

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const mandatoryValue = ['author', 'name', 'price'];

class UserError extends Error {
    constructor() {
        super();
        this.name = 'Your Error';
        this.message = 'It is necessary to enter a mandatory value (name, price, author)';
    }
};

class CreateList {
        constructor(name, author, price) {
            this.name = name;
            this.author = author;
            this.price = price;
        }
        render () {
            this.createElement();
        }
        createElement() {
            const divForList = document.getElementById('root');
            divForList.insertAdjacentHTML('afterbegin', `<ul>
                                                            <li>${this.name}</li>
                                                                <ul>
                                                                    <li>${this.author}</li>
                                                                    <li>${this.price}</li>
                                                                </ul>
                                                        </ul>`)
        }
    };

const handleArray = (array) => {
    array.forEach((element) => {
        const validForError = mandatoryValue.filter(elem => !element.hasOwnProperty(elem)).join('')
        try {
            if(validForError) {
                throw new UserError;
            }
            const create = new CreateList(element.author, element.name, element.price).render()       
        }
        catch (err) {
            console.log(err.name);
            console.log(err.message);
        }
    });
};

handleArray(books);

// //Вариант 2

// const books = [
//     {
//         author: "Скотт Бэккер",
//         name: "Тьма, что приходит прежде",
//         price: 70
//     },
//     {
//         author: "Скотт Бэккер",
//         name: "Воин-пророк",
//     },
//     {
//         name: "Тысячекратная мысль",
//         price: 70
//     },
//     {
//         author: "Скотт Бэккер",
//         name: "Нечестивый Консульт",
//         price: 70
//     },
//     {
//         author: "Дарья Донцова",
//         name: "Детектив на диете",
//         price: 40
//     },
//     {
//         author: "Дарья Донцова",
//         name: "Дед Снегур и Морозочка",
//     }
// ];

// const mandatoryValue = ['author', 'name', 'price'];

// class AuthorError extends Error {
//     constructor(name, message) {
//         super();
//         this.name = 'Author book';
//         this.message = 'не найдено обязательное свойство author';
//     }
// };
// class PricerError extends Error {
//     constructor(name, message) {
//         super();
//         this.name = 'Price book';
//         this.message = `не найдено обязательное свойство price`;
//     }
// };
// class NameError extends Error {
//     constructor(name, message) {
//         super();
//         this.name = 'Name book';
//         this.message = 'не найдено обязательное свойство name';
//     }
// };

// class CreateList {
//     constructor(name, author, price) {
//         this.name = name;
//         this.author = author;
//         this.price = price;
//     }
//     render () {
//         this.createElement();
//     }
//     createElement() {
//         const divForList = document.getElementById('root');
//         divForList.insertAdjacentHTML('afterbegin', `<ul>
//                                                         <li>${this.name}</li>
//                                                             <ul>
//                                                                 <li>${this.author}</li>
//                                                                 <li>${this.price}</li>
//                                                             </ul>
//                                                     </ul>`)
//     }
// };
//     const handleArray = (array) => {
//         array.forEach((element) => {       
//             try {
//                 if(!element.name) {
//                     throw new NameError('name');
//                 }   else if(!element.price){
//                     throw new PricerError('price');
//                 }   else if(!element.author) {
//                     throw new AuthorError('author')
//                 }
//                 const create = new CreateList(element.name, element.price, element.author).render()       
//             }
//             catch (err) {
//                 console.log(err.name, err.message);
//             }
//         })
//     }

// handleArray(books);

