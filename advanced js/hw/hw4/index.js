// Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.

// Технические требования:

// Отправить AJAX запрос по адресу https://ajax.test-danit.com/api/swapi/films и получить список всех фильмов серии Звездные войны

// Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
// Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
// Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.

const requestURL = 'https://ajax.test-danit.com/api/swapi/films'

function getData(array) {
	console.log(array)
	array.sort((a, b) => (a.episodeId > b.episodeId ? 1 : -1))
	return array.map(arrayFilms => {
		const { episodeId, openingCrawl, name, characters } = arrayFilms
		const ul = document.createElement('ul')
		console.log(episodeId)
		ul.classList.add('list')
		ul.setAttribute('data-film', episodeId)
		ul.id = `${episodeId}`
		ul.insertAdjacentHTML(
			'afterbegin',
			`	<li class="item_name">Films : ${name}</li>
				<li class="item_episode">Episode: ${episodeId}</li>
				<li class="item_opening">Opening: ${openingCrawl}</li>
				<h1 class="title">Characters</h1>
			`
		)
		document.body.append(ul)
		return characters
	})
}

function arrCharacter(arr) {
	arr.forEach((arrayCh, index) => {
		arrayCh.forEach(linkCharacter => {
			fetch(linkCharacter)
				.then(response => {
					return response.json()
				})
				.then(characterName => {
					addCharacterToList(characterName.name, index)
				})
		})
	})
}

function addCharacterToList(characterName, index) {
	const ul = document.querySelectorAll('ul')
	ul.forEach(elem => {
		if (+elem.dataset.film === index + 1)
			elem.insertAdjacentHTML(
				`beforeend`,
				`<li class='characters'>${characterName}</li>`
			)
	})
}

fetch(requestURL)
	.then(response => response.json())
	.then(getData)
	.then(arrCharacter)
