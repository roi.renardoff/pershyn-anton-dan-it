// Написать программу "Я тебя по айпи вычислю"
// Технические требования:
// Создать простую HTML страницу с кнопкой Вычислить по IP.
// По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
// Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
// Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
// Все запросы на сервер необходимо выполнить с помощью async await.

// создаем функцию получения ip
async function getIp() {
	const requestIp = 'https://api.ipify.org/?format=json'
	const response = await fetch(requestIp)
	const { ip } = await response.json()
	// console.log(ip)
	getData(ip)
}

async function getData(ip) {
	const requestData = `http://ip-api.com/json/${ip}`
	const response = await fetch(requestData)
	const data = await response.json()
	// console.log(data)
	addToPage(data)
}

function addToPage({ timezone, country, region, city, regionName }) {
	const ul = document.createElement('ul')
	ul.insertAdjacentHTML(
		'afterbegin',
		`	<li>${timezone}</li>
			<li>${country}</li>
			<li>${region}</li>
			<li>${city}</li>
			<li>${regionName}</li>
		`
	)
	document.body.append(ul)
}

const button = document.querySelector('button')
button.addEventListener('click', getIp)
