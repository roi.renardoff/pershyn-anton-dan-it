class VisitCardiologist extends Visit {
  constructor({
    target,
    description,
    urgency,
    name,
    pressure,
    BMI,
    deseases,
    age,
  }) {
    super({ target, description, urgency, name });
    this.pressure = pressure;
    this.BMI = BMI;
    this.deseases = deseases;
    this.age = age;
  }
  render(container) {
    //   создать модальное окно
    this.createModal();
    // положить в модальное окно форму
    this.insertIntoPage(container);
  }
}
