class Visit {
  constructor({ target, description, urgency, name }) {
    this.target = target;
    this.description = description;
    this.urgency = urgency;
    this.name = name;
  }

  render() {
    this.createModal();
  }
  createModal() {
    const modal = new Modal({
      classes: ["modal", "open"],
      title: "create visit",
      content: new Form({
        classes: ["form"],
        children: this.getFormInputs(),
        btnSubmit: this.getBtnSubmit(),
      }),
    });
    console.log(modal);
    modal.render();
  }
  getTargetInput() {
    return new Input({
      classes: ["input"],
      placeholder: "Введите цель визита",
      type: "text",
    });
  }
  getDescriptionInput() {
    return new Input({
      classes: ["input"],
      placeholder: "Введите описание визита",
      type: "text",
    });
  }

  //   getUrgencyInput() {
  //     return new Select({
  //       //   classes: ["input"],
  //       //   placeholder: "Введите срочность визита",
  //       //   type: "text",
  //     });
  //   }

  getNameInput() {
    return new Input({
      classes: ["input"],
      placeholder: "Введите ваше ФИО",
      type: "text",
    });
  }

  getFormInputs() {
    return [
      this.getTargetInput(),
      this.getDescriptionInput(),
      //   this.getUrgencyInput(),
      this.getNameInput(),
    ];
  }

  getBtnSubmit() {
    return new Button({
      classes: [""],
    });
  }
}
