class Modal extends HtmlElement {
  constructor({ classes, title, content }) {
    super(classes);
    this.title = title;
    this.content = content;
  }

  render(container) {
    this.createModal();
    this.addCloseModalListener();
    this.insertIntoPage(container);
  }

  createModal() {
    const modalLayout = `
         <span class="modal-close">x</span>
         <h3>${this.title}</h3>
      `;

    this.createElement({
      element: "div",
      attributes: {
        innerHTML: modalLayout,
      },
    });

    this.content.render(this.element);
  }

  addCloseModalListener() {
    this.element.addEventListener("click", (event) => {
      if (event.target.classList.contains("modal-close")) {
        // this.element.remove();
        this.element.classList.remove("open");
      }
    });
  }

  insertIntoPage(container = document.body) {
    container.append(this.element);
  }
}
