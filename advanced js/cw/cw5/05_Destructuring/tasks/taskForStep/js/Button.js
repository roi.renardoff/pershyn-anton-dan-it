class Button extends HtmlElement {
  constructor({ classes, type, text, clickHandler }) {
    super(classes);
    this.type = type;
    this.text = text;
    this.clickHandler = clickHandler;
  }
  render(container) {
    this.createButton();
    this.insertIntoPage(container);
    this.addClickHandler();
  }
  createButton() {
    this.createElement({
      element: "button",
      attributes: {
        type: this.type,
        innerHTML: this.text,
      },
    });
  }
  addClickHandler() {
    this.element.addEventListener("click", this.clickHandler);
  }
}
