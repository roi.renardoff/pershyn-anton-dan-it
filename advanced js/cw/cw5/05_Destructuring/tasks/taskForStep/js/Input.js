class Input extends HtmlElement {
  constructor({ classes, placeholder, type }) {
    super(classes);
    this.placeholder = placeholder;
    this.type = type;
  }
  render(container) {
    this.createInput();
    this.insertIntoPage(container);
  }

  createInput() {
    this.createElement({
      element: "input",
      attributes: {
        type: this.type,
        placeholder: this.placeholder,
      },
    });
  }
}
