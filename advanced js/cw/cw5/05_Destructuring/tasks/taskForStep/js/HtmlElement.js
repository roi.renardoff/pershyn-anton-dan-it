class HtmlElement {
  constructor(classes) {
    this.classes = classes;
    this.element = null;
  }

  // elementData: {
  //     element: 'button',
  //     attibutes: {
  //         innerHtml: 'Hello'
  //     }
  // }

  createElement(elementData) {
    this.element = document.createElement(elementData.element);
    this.element.className = this.classes.join(" ");
    // ['innerHtml', 'placeholder']
    Object.keys(elementData.attributes).forEach((key) => {
      //  this.element.placeholder = elementData.attributes.placeholder
      this.element[key] = elementData.attributes[key];
    });
  }

  insertIntoPage(container) {
    container.append(this.element);
  }
}
