class Form extends HtmlElement {
  constructor({ classes, children, btnSubmit }) {
    super(classes);
    this.children = children;
    this.btnSubmit = btnSubmit;
  }

  render(container) {
    this.createForm();
    this.addInputsToForm();
    this.addBtnSubmit();
    this.insertIntoPage(container);
  }

  createForm() {
    this.createElement({
      element: "form",
      attributes: {},
    });
  }

  addInputsToForm() {
    this.children.forEach((input) => {
      input.render(this.element);
    });
  }

  addBtnSubmit() {
    this.btnSubmit.render(this.element);
  }
}
