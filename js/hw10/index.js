// По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.

// Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)

// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)

// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях

// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;

// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения


// После нажатия на кнопку страница не должна перезагружаться

// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.


    let iconPassword = document.querySelectorAll('.fa-eye');
let messageErrorPassword = document.querySelector('.warning');
let formOrPassword = document.forms[0];
let firstInputInForm = formOrPassword.first;
let secondInputInForm = formOrPassword.second;
let lengthPasswordError = document.querySelector('.span');

const  checkLengthPassword = () => {
    if (firstInputInForm.value.length < 6) {
        lengthPasswordError.classList.add('error-password');
    } else {
        lengthPasswordError.classList.remove('error-password');
    }
};

firstInputInForm.addEventListener('input', checkLengthPassword);

formOrPassword.onsubmit = () => false;

const checkPasswordInInput = (event) => {
    if (firstInputInForm.value.length < 6){
        event.preventDefault();
    }   else if (firstInputInForm.value === secondInputInForm.value && firstInputInForm.value !== '' && secondInputInForm.value !=='' ) {
        alert('You are welcome');
        formOrPassword.reset();
    } else {
        messageErrorPassword.classList.add('warning-message');
        event.preventDefault();
    }
}

formOrPassword.addEventListener('submit', checkPasswordInInput);


let showHide = (event) => {
    if (event.target.closest('.input-wrapper').querySelector('input').getAttribute('type') === 'text') {
        event.target.closest('.input-wrapper').querySelector('input').setAttribute('type', 'password');
    } else {
        event.target.closest('.input-wrapper').querySelector('input').setAttribute('type', 'text');
    };
    event.target.classList.toggle('fa-eye-slash');
};

iconPassword.forEach(element => {
    element.addEventListener('click', showHide);
});