//Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
//Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
//Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя. Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
//Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.
//Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


let userName = prompt('Ваше имя');

while (!userName) {
    userName = prompt('Ваше имя');
}

let userAge = +prompt('Ваш возраст');

while (isNaN(userAge) || userAge == '') {
    userAge = +prompt('Ваш возраст');
}

const agreeAge = 18;

if (userAge < agreeAge) {
    alert('You are not allowed to visit this website.');
}
else if (userAge >= agreeAge && userAge <= 22) {
    confirm('Are you sure you want to continue?') ? alert(`Welcome, ${userName}`) : alert('You are not allowed to visit this website.');
}
else {
    alert(`Welcome, ${userName}`);
}




