// В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.


// Вариант 1
const tabs = document.querySelectorAll('.tabs-title');

tabs.forEach(function (tab) {
  tab.addEventListener('click', function () {

    let currentTabData = document.querySelector('.content[data-content="' + this.dataset.item + '"]');
    // remove classess
    document.querySelector('.content.active').classList.remove('active');
    document.querySelector('.tabs-title.active').classList.remove('active');
    // add classes
    currentTabData.classList.add('active');
    this.classList.add('active');
  });
})


//Вариант 2
// function clickTab(event) {
//   let currentTabData = document.querySelector('.content[data-content="' + event.target.dataset.item + '"]');
//   event.target.closest('.tabs').querySelector('.active').classList.remove('active');
//   currentTabData.closest('.tabs-content').querySelector('.active').classList.remove('active')
//   currentTabData.classList.add('active');
//   event.target.classList.add('active');
// }

// document.addEventListener('click', clickTab)



