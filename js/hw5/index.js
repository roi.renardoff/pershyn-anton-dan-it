// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

function createNewUser() {
    firstName = prompt('Введите Ваше имя');
    while (firstName == '') {
        firstName = prompt('Введите Ваше имя еще раз');
    }

    lastName = prompt('Введите Вашу фамилию');
    while (lastName == '') {
        lastName = prompt('Введите Вашу фамилию еще раз');
    }

    userBirthday = prompt('Введите свою дату рождения в формате dd.mm.yyyy');
    while (userBirthday == '') {
        userBirthday = prompt('Введите свою дату рождения в формате dd.mm.yyyy ещё раз')
    }
    return {
        firstName,
        lastName,
        userBirthday,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },

        getBirthdayYear() {
            return this.userBirthday.slice(6, 10);
        },

        getAge() {
            return new Date().getFullYear() - this.getBirthdayYear();
        },

        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.getBirthdayYear();
        }
    }
}

let newUser = new createNewUser();
console.log(newUser.getLogin(), newUser.getAge(), newUser.getPassword());