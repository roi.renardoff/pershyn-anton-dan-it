// Создать массив объектов students (в количестве 7 шт).
// У каждого студента должно быть имя, фамилия и направление обучения - Full-stask
// или Front-end. У каждого студента должен быть метод, sayHi, который возвращает строку
// `Привет, я ${имя}, студент Dan, направление ${направление}`.
// Перебрать каждый объект и вызвать у каждого объекта метод sayHi;

// const student = [
//     {
//         name: 'Anton',
//         lastName: 'Pershyn',
//         direction: 'front-end',
//         sayHi(){
//             return `Привет, я ${this.имя}, студент Dan, направление ${this.direction}`
//         }

//     },

//     {
//         name: 'Ira',
//         lastName: 'Pershyn',
//         direction: 'ffull-stack',
//         sayHi(){
//             return `Привет, я ${this.имя}, студент Dan, направление ${this.direction}`
//         }

//     },

//     {
//         name: 'Andrey',
//         lastName: 'Pershyn',
//         direction: 'front-end',
//         sayHi(){
//             return `Привет, я ${this.имя}, студент Dan, направление ${this.direction}`
//         }

//     },
// ];
// student.forEach(Element => {
//     console.log(Element.sayHi())
// });


// Есть массив брендов автомобилей ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам нужно получить новый массив, объектов типа
// {
//     type: 'car'
//     brand: ${элемент массива}
// }

// Вывести массив в консоль

// const brands = ["bMw", "Audi", "teSLa", "toYOTA"];
// const transformedDrands = brands.map((Element)=>{
//     return {
//         type: 'car',
//         brand: Element,
//     }
// })

// console.log(transformedDrands);


// Создать массив чисел от 1 до 100.
// Отфильтровать его таким образом, чтобы в новый массив не попали числа меньше 10 и больше 50.
// Вывести в консоль новый массив.

// const numbers = []
// for (let index = 1; index <= 100; index++) {
//     if(index >= 10 || index <= 50){
//     numbers.push(index)
//     }
// }
// console.log(numbers);


// Создать функцию, которая проверяет, является ли переданная в нее строка
// палиндромом (анна это паллиндром).
// Подсказка: нужно преобразовать строку в массив

// const isPalindrom = (string) => {
     // const arrayFromString = string.split('');
     // const reverseArray = arrayFromString.reverse();
     // const reverseToString = reverseArray.join('');
     // return string == reverseToString;
     // оптимизация кода
     //1.делаем из строки массив симыолом методом сплит
     //2.переворачиваем в обратном порядке(reverse)
     //3.полученный массив преобразованный в массив возвращаем в строку методом join
     //4.приравниваем к строке и сравниваем 
//     return string.split('').reverse('').join('') === string;
// }

// Для всех машин добавить свойство isElectric, для теслы оно будет true, для остальных false;
// Для того чтоб проверять что машина электрическая создать отдельную функцию что будет хранить
// в себе массив брендов электрических машин и будет проверять входящий элемент - на то,
// если он в массиве.

// Создать новый массив, в котором хранятся все машины, что не являются электрокарами.
// Вывести его в модальное окно пользователю.

// const carBrands = ["bMw", "Audi", "teSLa", "toYOTA"];
// const isElectric = (brand) => {
//     const electricCars = ['tesla', 'nissan'];
//     return electricCars.includes(brand.toLowerCase());
// }
// const carBrandsObject = carBrands.map((Element) => {
//     return {
//         type: 'car',
//         brand: Element,
//         isElectric: isElectric(Element),
//     }
// });
// const electricCars = carBrandsObject.filter((Element) => {
//     return !Element.isElectric
// });
// console.log(electricCars);



// Описание задачи: Напишите функцию, которая очищает массив от нежелательных значений,
// таких как false, undefined, пустые строки, ноль, null.

// const data = [0, 1, false, 2, undefined, '', 3, null];
// console.log(compact(data)) // [1, 2, 3]
     // создаем функцию, которая создает массив
// const filterNullishValues = (arr) => {
     // создаем массив, содержащий нежелательные значения
     // const nullishValues = [false, null, undefined, '', 0]
     // вызываем метод массива фильтр, которому передаем условие для фильтрации элементов вызовом инклудес для массива с нежелательным элементами и в него передаем элементы проверяемого массива
     // если инклудс тру то конвертируем его в фалс, чтобы фильтр не включил элементы в новый массив
//     return arr.filter(Element => !nullishValues.includes(Element));
// } 
// const data = [0, 1, false, 2, undefined, '', 3, null];
// console.log(filterNullishValues(data));


// Описание задачи: Напишите функцию, которая сравнивает два массива и возвращает true,
// если они идентичны.
// Ожидаемый результат: ([1, 2, 3], [1, 2, 3]) => true

// const arr1 = [1, 2, 3, 4];
// const arr2 = [1, 2, 3, 4];
// const arr3 = [1, 2, 3, 5];
// const arr4 = [1, 2, 3, 4, 5];
// console.log(isEqual(arr1, arr2)); // true
// console.log(isEqual(arr1, arr3)); // false
// console.log(isEqual(arr1, arr4)); // false

// const isArraysEqual = (arr1, arr2) => {
//     if (arr1.lenght !== arr2.lenght) return false;
//     for (let i = 0; i < arr1.lenght; i++) {
//         if (arr1[i] === arr2[i]) return false;
//     }
//     return true;
// }
// оптимизация кода
const isArraysEqual = (arr1, arr2) => {
        return arr1.lenght === arr2.lenght && srr1.every((Element,index) => Element === arr2[index]);
}
const arr1 = [1, 2, 3, 4];
const arr2 = [1, 2, 3, 4];
const arr3 = [1, 2, 3, 5];
const arr4 = [1, 2, 3, 4, 5];
console.log(isEqual(arr1, arr2));
console.log(isEqual(arr1, arr3));
console.log(isEqual(arr1, arr4));

