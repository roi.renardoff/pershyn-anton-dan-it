let userArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function listPage (arr, documentPage = document.body) {
    documentPage.insertAdjacentHTML('afterbegin', `<ul>${arr.map(elem => {
        return `<li>${elem}</li>`
    }).join('')}</ul>`);
}

listPage(userArr);
