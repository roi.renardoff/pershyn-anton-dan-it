// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].

let userArr = ['hello', 23, null, undefined, function () { }, true, {}, 10n];

let userTypeOf = ['number', 'string', 'boolean', 'undefined', 'symbol', 'object', 'null', 'bigint'];

function filterBy(arr, typeOf) {
    let userArrFiltered = new Array();
    arr.forEach(element => {
        if (!(typeof element === typeOf)) {
            userArrFiltered.push(element);
        }
    });
    return userArrFiltered;
}
userTypeOf.forEach(elem => console.log(filterBy(userArr, elem)));