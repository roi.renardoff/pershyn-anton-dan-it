const keyTheme = document.getElementById('themeBtn');

function changeTheme () {
    document.body.classList.toggle('dark');
    keyTheme.innerText = document.body.classList.contains('dark') ? 'Light' : 'Dark'
    localStorage.activeTheme = document.body.className || ''
    
}

function checkLocalStorage () {
if(!localStorage.activeTheme) 
localStorage.activeTheme = ''
document.body.className = localStorage.activeTheme
document.getElementById('themeBtn').innerText = document.body.classList.contains('dark') ? 'Light' : 'Dark';
    }


keyTheme.addEventListener('click', changeTheme);
checkLocalStorage();