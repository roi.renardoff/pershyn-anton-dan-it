// ## Задание
// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
// #### Технические требования:
//    - При загрузке страницы показать пользователю поле ввода (`input`) с надписью `Price`. Это поле будет служить для ввода числовых значений

//    - Поведение поля должно быть следующим:

//    - При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.

//    - Когда убран фокус с поля - его значение считывается, над полем создается `span`, в котором должен быть выведен текст: `Текущая цена: ${значение из поля ввода}`.

//    - Рядом с ним должна быть кнопка с крестиком (`X`). Значение внутри поля ввода окрашивается в зеленый цвет.

//    - При нажатии на `Х` - `span` с текстом и кнопка `X` должны быть удалены. Значение, введенное в поле ввода, обнуляется.

//    - Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - `Please enter correct price`. `span` со значением при этом не создается.


const inputPrise = document.querySelector('.input-prise');

inputPrise.onfocus = function () {
    inputPrise.classList.add('valid');
}

inputPrise.onblur = function () {
    const priseValue = document.querySelector('.input-prise').value;
    const errorInput = document.querySelector('.by-span')
    if (priseValue == "") {
        this.classList.remove('valid')
    } else if (priseValue <= 0) {
    errorInput.classList.add('incorrect-value');
    this.classList.add('invalid');
    this.classList.remove('valid')
    } else {
        document.querySelector('.input-block').insertAdjacentHTML('afterbegin', `<span class="price-label">Текущая цена, ${document.querySelector('.input-prise').value} <button class="btn-close" onclick="deletePrise(this)">X</button></span>`);
        errorInput.classList.remove('incorrect-value');
        this.classList.remove('invalid');
        this.classList.add('valid');
    }
}

const deletePrise = (element) => {
    element.closest('.price-label').remove()
    document.querySelector('.input-prise').value = "";
    inputPrise.classList.remove('valid');
}

