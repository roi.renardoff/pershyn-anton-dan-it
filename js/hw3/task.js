let userNumberOne = +prompt('Введите первое число');
while (isNaN(userNumberOne) || userNumberOne == '') {
    userNumberOne = +prompt('Введите первое число еще раз');
}

let userNumberTwo = +prompt('Введите второе число');
while (isNaN(userNumberTwo) || userNumberTwo == '') {
    userNumberTwo = +prompt('Введите второе число еще раз');
}

let yourOperation = prompt('Введите операцию, которую хотите совершить *, -, +, /');
while (yourOperation !== "*" && yourOperation !== "-" && yourOperation !== "+" && yourOperation !== "/") {
    yourOperation = prompt('Введите корректное значение  *, -, +, / ');
}


function calc(userNumberOne, userNumberTwo, action) {
    switch (action) {
        case '+':
            return userNumberOne + userNumberTwo
        case '-':
            return userNumberOne - userNumberTwo
        case '*':
            return userNumberOne * userNumberTwo
        case '/':
            return userNumberOne / userNumberTwo
    }
}

let result = calc(userNumberOne, userNumberTwo, yourOperation);
alert(result);